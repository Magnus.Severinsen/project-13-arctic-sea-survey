# Project 13 - Arctic sea survey

## Interactive Map



Here we will quickly go over all the key elements of the directory.
Starting out with looking at the different files and folders that is used.

- analysis.py is a python script that has been used and can still be used to check and validate coordinates for a ships path. Note that you need all the packages downloaded for it to function correctly.
- old.py is the older version of the same python script, but it can be practical to have available in case there is download issues for the dependancies for analysis.py.
- `quardatic.jpg` is the background image that is used in old.py map function
- db is the directory where we have located the database with the file sasdb.sql that has the database setup for the project.
- `"Files for new SAS map"` is a directory that was given to us with paths and logs of the coordinates different ships have traveled throughout missions.
- `Missions` is a directory with the collection of missions coordinates that is in the correct format to be utilized in the webpage
- `All_ship_names.txt`, All_ships_altered.txt and Araon.txt are collections of csv file names that is wanted to have a closer look on in `analysis.py` and `old.py`
- `map-generator.ipynb` is the file that writes the html file (`index.html`) corresponding with the interactive map. .ipynb or Notebook files are run by simply hitting enter, or hitting the run all cells
- `index.html` is the HTML file that contains the interactive map object.
https://imgur.com/a/LZcgq2W





Collaberators: Jakob Torp Maaseide and Magnus Høydahl Severinsen
