Files are saved as ship_year.csv (comma delimited) with data retrieved from overviews sent by chief scientists.

Araon_2020
as sent by Are on 2023.06.08

Armstrong_2022
contains only stations with phys, chem and bio, as CTD-only stations had only planned positions and not measured positions.

Dana_2020
contains more than 6 decimals for certain values due to conversion from degrees to decimal degrees.

Oden_2021
contains average latitude and longitude (DD) from expedition logbook, tab 3. All 271 divece operations column I and J.
