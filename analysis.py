from mpl_toolkits.basemap import Basemap
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd



# Uses the file with all filenames to prepare a list of all the calls that should be made later on
def makeShipList(filename):
    ship_list = []
    f = open(filename, "r")
    for names in f.readlines():
        names = names.replace("\n", "")
        ship_list.append("Files for new SAS map/" + names)
    return ship_list


# Exctracting data points from files, and converting it to the correct projection
def extractDataPoints(filename):

    df = pd.read_csv(filename, sep=";")
    # changing the values to make the map placement correct
    xValue = df['lat']                 # Converting latitude to be from 0 and up instead of 90 and down to visualize it
    yValue = df['lon']                 # Converting degrees of longitude to radians to visualize it correctly in the polar plot

    return xValue, yValue

def plotSetUp():

    m = Basemap(projection='npstere',boundinglat=60,lon_0=0,resolution='i')

    m.drawcoastlines(linewidth=0.2)
    m.fillcontinents(color='PapayaWhip', lake_color='PowderBlue')

    # draw parallels and meridians.
    m.drawparallels(np.arange(-80., 81., 20.), linewidth=0.2)
    m.drawmeridians(np.arange(-180., 181., 20.), linewidth=0.2)
    m.drawmapboundary(fill_color='PowderBlue')

    # draw parallels and meridians.
    m.drawparallels(np.arange(-80., 81., 20.), linewidth=0.2)
    m.drawmeridians(np.arange(-180., 181., 20.), linewidth=0.2)
    # draw tissot's indicatrix to show distortion.
    return m

# Make a plot from a single file
def makePlotFromFile(name):
    m = plotSetUp()
    xValue, yValue = extractDataPoints(name)
    lons, lats = m(yValue, xValue)
    # plot points as red dots
    m.scatter(lons,lats, marker='.', zorder=0.01)
    m.plot(lons,lats)
    ax = plt.gca()
    plt.show()

# Make a plot from a list of filenames
def makePlotFromList(lst):
    #fig, ax, ax_image = plotSetUp()
    m = plotSetUp()
    for name in lst:
        xValue, yValue = extractDataPoints(name)
        lons, lats = m(yValue, xValue)
        # plot points as red dots
        m.scatter(lons, lats, marker='.', zorder=0.1)
        m.plot(lons, lats)
        ax = plt.gca()

    # ax.legend()
    plt.show()

def editFile(filename):
    f = open("Files for new SAS map/" + filename, "r")
    new = open("Files for new SAS map/" + filename[:-4] + "_altered.csv", "a")

    for line in f.readlines():
        if line[:-3] == ".000":
            pass
        new.write(line.replace(",", "."))

    f.close()
    new.close()


def main():
    Ship_list = makeShipList("Files for new SAS map/All_ship_altered.txt")
    # Choose between showing one or multiple paths
    makePlotFromList(Ship_list)
    #makePlotFromFile("Files for new SAS map/Araon_2021.csv")


if __name__ == '__main__':
    main()

