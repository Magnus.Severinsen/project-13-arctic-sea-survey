import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

# Uses the file with all filenames to prepare a list of all the calls that should be made later on
def makeShipList(filename):
    ship_list = []
    f = open(filename, "r")
    for names in f.readlines():
        names = names.replace("\n", "")
        ship_list.append("Files for new SAS map/" + names)
    return ship_list


# Exctracting data points from files, and converting it to the correct projection
def extractDataPoints(filename):

    df = pd.read_csv(filename, sep=";")
    # changing the values to make the map placement correct
    xValue = df['lat']                 # Converting latitude to be from 0 and up instead of 90 and down to visualize it
    yValue = np.deg2rad(df['lon'])        # Converting degrees of longitude to radians to visualize it correctly in the polar plot

    return xValue, yValue

# Setting up the plot settings to make it consistant between the different ways of visualizing
def plotSetUp():
    fig = plt.gcf()
    axis_coords = [0,0,1,1]

    img = plt.imread("quardatic.jpg")
    ax_image = fig.add_axes(axis_coords, label="ax image")
    ax_image.imshow(img)
    ax_image.axis('off')

    ax = fig.add_axes(axis_coords, projection='polar', label="ax polar")

    ax.patch.set_alpha(0)
    ax.set_ylim(90,64.7)

    # Offset to make the points match the actual position on the map
    ax.set_theta_offset(250.5 * np.pi / 180)

    return fig, ax, ax_image


# Make a plot from a single file
def makePlotFromFile(name):
    fig, ax, ax_image = plotSetUp()
    xValue, yValue = extractDataPoints(name)

    ax.plot(yValue, xValue, '.')
    ax.plot(yValue, xValue, label=name[22:-4])
    plt.show()

# Make a plot from a list of filenames
def makePlotFromList(lst):
    fig, ax, ax_image = plotSetUp()
    for name in lst:
        xValue, yValue = extractDataPoints(name)
        ax.plot(yValue, xValue, '.')
        ax.plot(yValue, xValue, label=name[22:-4])

    plt.show()

def editFile(filename):
    f = open("Files for new SAS map/" + filename, "r")
    new = open("Files for new SAS map/" + filename[:-4] + "_altered.csv", "a")

    for line in f.readlines():
        if line[:-3] == ".000":
            pass
        new.write(line.replace(",", "."))

    f.close()
    new.close()


def analysis():
    Ship_list = makeShipList("Files for new SAS map/All_ship_altered.txt")
    # Choose between showing multiple or a single path
    makePlotFromList(Ship_list)
    # makePlotFromFile("Files for new SAS map/Mirai_2021.csv")

if __name__ == '__main__':
    analysis()

