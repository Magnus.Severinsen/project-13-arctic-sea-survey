-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 08. Des, 2023 12:29 PM
-- Tjener-versjon: 10.4.32-MariaDB
-- PHP Version: 8.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sasdb`
--

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `contactperson`
--

CREATE TABLE `contactperson` (
  `idContact` int(11) NOT NULL,
  `contactName` varchar(45) DEFAULT NULL,
  `emailAddress` varchar(45) DEFAULT NULL,
  `institution` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dataark for tabell `contactperson`
--

INSERT INTO `contactperson` (`idContact`, `contactName`, `emailAddress`, `institution`) VALUES
(1, 'Kyoung-Ho Cho', 'kcho@kopri.re.kr', ''),
(2, 'Kumiko Azetsu-Scott', 'kumiko.azetsu-scott@dfo-mpo.gc.ca', ''),
(3, 'Bill Williams', 'bill.williams@dfo-mpo.gc.ca', ''),
(4, 'Mary-Louise Timmermans', 'mary-louise.timmermans@yale.edu', ''),
(5, 'Shigeto Nishino', 'nishinos@jamstec.go.jp', ''),
(6, 'Amane Fujiwara', 'amane@jamstec.go.jp', ''),
(7, 'Motoyo Itoh', 'motoyo@jamstec.go.jp', ''),
(8, 'Marja Koski', 'mak@aqua.dtu.dk', ''),
(9, 'Marit Reigstad', 'marit.reigstad@uit.no', ''),
(10, 'Agneta Fransson', 'agneta.fransson@npolar.no', ''),
(11, 'Are Olsen', 'are.olsen@uib.no', ''),
(12, 'Vidar Lien & Melissa Chierici', 'vidar.lien@hi.no, melissa.chierici@hi.no', ''),
(13, 'Vidar Lien & Melissa Chierici', 'vidar.lien@hi.no', ''),
(14, 'Dmitry Prozorkevich', 'dvp@pinro.ru', ''),
(15, 'Søren Rysgaard', 'rysgaard@au.dk', ''),
(16, 'Heidi Kassens', 'hkassens@geomar.de', ''),
(17, 'Maurizio Azzaro', 'maurizio.azzaro@cnr.it', ''),
(18, 'Pauline Leijonmalm', 'pauline.snoeijs-leijonmalm@su.se', ''),
(19, 'Carin Ashjian and Jackie Grebmeier', 'jgrebmei@umces.edu', ''),
(20, 'Igor Polyakov', 'uaf-atmos@alaska.edu', '');

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `coordinate`
--

CREATE TABLE `coordinate` (
  `coordinateNr` int(11) NOT NULL,
  `missionId` int(11) NOT NULL,
  `datetime` date DEFAULT NULL,
  `latitude` decimal(5,0) NOT NULL,
  `longitude` decimal(5,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dataark for tabell `coordinate`
--

INSERT INTO `coordinate` (`coordinateNr`, `missionId`, `datetime`, `latitude`, `longitude`) VALUES
(1, 1, NULL, 65, -169),
(1, 10, NULL, 47, 160),
(2, 1, NULL, 66, -168),
(2, 10, NULL, 72, 205),
(3, 1, NULL, 67, -169),
(3, 10, NULL, 72, 205),
(4, 1, NULL, 67, -169),
(4, 10, NULL, 72, 205),
(5, 1, NULL, 68, -169),
(5, 10, NULL, 72, 205),
(6, 1, NULL, 68, -169),
(6, 10, NULL, 72, 204),
(7, 1, NULL, 68, -168),
(7, 10, NULL, 72, 204),
(8, 1, NULL, 68, -168),
(8, 10, NULL, 72, 204),
(9, 1, NULL, 68, -167),
(9, 10, NULL, 72, 204),
(10, 1, NULL, 68, -167),
(10, 10, NULL, 72, 204),
(11, 1, NULL, 68, -169),
(11, 10, NULL, 75, 198),
(12, 1, NULL, 69, -169),
(12, 10, NULL, 73, 200),
(13, 1, NULL, 70, -169),
(13, 10, NULL, 72, 200),
(14, 1, NULL, 70, -169),
(14, 10, NULL, 73, 199),
(15, 1, NULL, 71, -169),
(15, 10, NULL, 73, 202),
(16, 1, NULL, 71, -169),
(16, 10, NULL, 73, 203),
(17, 1, NULL, 72, -169),
(17, 10, NULL, 73, 201),
(18, 1, NULL, 72, -169),
(18, 10, NULL, 73, 205),
(19, 1, NULL, 73, -169),
(19, 10, NULL, 73, 205),
(20, 1, NULL, 74, -168),
(20, 10, NULL, 72, 207),
(21, 1, NULL, 74, -168),
(21, 10, NULL, 72, 205),
(22, 1, NULL, 75, -168),
(22, 10, NULL, 72, 203),
(23, 1, NULL, 76, -171),
(23, 10, NULL, 72, 201),
(24, 1, NULL, 77, -170),
(24, 10, NULL, 72, 199),
(25, 1, NULL, 78, -170),
(25, 10, NULL, 72, 198),
(26, 1, NULL, 77, -175),
(26, 10, NULL, 71, 199),
(27, 1, NULL, 76, -176),
(27, 10, NULL, 71, 195),
(28, 1, NULL, 76, 180),
(28, 10, NULL, 72, 197),
(29, 1, NULL, 76, 178),
(29, 10, NULL, 72, 194),
(30, 1, NULL, 77, 179),
(30, 10, NULL, 72, 191),
(31, 1, NULL, 77, 180),
(31, 10, NULL, 72, 192),
(32, 1, NULL, 77, -177),
(32, 10, NULL, 72, 191),
(33, 1, NULL, 78, -175),
(33, 10, NULL, 71, 193),
(34, 1, NULL, 78, 180),
(34, 10, NULL, 71, 191),
(35, 1, NULL, 80, 172),
(35, 10, NULL, 71, 191),
(36, 1, NULL, 80, 172),
(36, 10, NULL, 70, 191),
(37, 1, NULL, 79, 173),
(37, 10, NULL, 70, 193),
(38, 1, NULL, 78, 173),
(38, 10, NULL, 70, 196),
(39, 1, NULL, 78, 173),
(39, 10, NULL, 69, 194),
(40, 1, NULL, 78, 173),
(40, 10, NULL, 69, 193),
(41, 1, NULL, 77, 174),
(41, 10, NULL, 69, 191),
(42, 1, NULL, 76, 174),
(42, 10, NULL, 69, 191),
(43, 1, NULL, 76, 174),
(43, 10, NULL, 69, 193),
(44, 1, NULL, 76, 174),
(44, 10, NULL, 69, 191),
(45, 1, NULL, 75, 175),
(45, 10, NULL, 68, 191),
(46, 1, NULL, 75, 175),
(46, 10, NULL, 68, 191),
(47, 1, NULL, 74, 174),
(47, 10, NULL, 68, 191),
(48, 1, NULL, 73, 169),
(48, 10, NULL, 68, 192),
(49, 1, NULL, 74, 170),
(49, 10, NULL, 68, 192),
(50, 1, NULL, 74, 171),
(50, 10, NULL, 68, 192),
(51, 1, NULL, 75, 172),
(51, 10, NULL, 68, 193),
(52, 1, NULL, 75, 174),
(52, 10, NULL, 68, 193),
(53, 1, NULL, 75, 175),
(53, 10, NULL, 68, 193),
(54, 1, NULL, 76, 176),
(54, 10, NULL, 68, 193),
(55, 1, NULL, 76, 177),
(55, 10, NULL, 67, 193),
(56, 1, NULL, 75, 177),
(56, 10, NULL, 67, 191),
(57, 1, NULL, 75, 178),
(57, 10, NULL, 66, 191),
(58, 1, NULL, 75, 190),
(58, 10, NULL, 66, 191),
(59, 1, NULL, 75, -178),
(59, 10, NULL, 66, 191),
(60, 1, NULL, 75, -176),
(60, 10, NULL, 65, 190),
(61, 1, NULL, 75, -174),
(61, 10, NULL, 65, 190),
(62, 1, NULL, 75, -172),
(62, 10, NULL, 64, 189),
(63, 1, NULL, 75, -170),
(64, 1, NULL, 75, -168),
(65, 1, NULL, 75, -167),
(66, 1, NULL, 76, -167),
(67, 1, NULL, 76, -165),
(68, 1, NULL, 77, -164),
(69, 1, NULL, 77, -164),
(70, 1, NULL, 77, -164),
(71, 1, NULL, 78, -164),
(72, 1, NULL, 78, -164),
(73, 1, NULL, 78, -171),
(74, 1, NULL, 79, -168),
(75, 1, NULL, 79, -165),
(76, 1, NULL, 79, -162),
(77, 1, NULL, 79, -161),
(78, 1, NULL, 79, -158),
(79, 1, NULL, 78, -159),
(80, 1, NULL, 76, -156),
(81, 1, NULL, 76, -160),
(82, 1, NULL, 76, -161),
(83, 1, NULL, 74, -160),
(84, 1, NULL, 74, -160),
(85, 1, NULL, 73, -160),
(86, 1, NULL, 74, -162),
(87, 1, NULL, 75, -164),
(88, 1, NULL, 75, -166);

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `country`
--

CREATE TABLE `country` (
  `idCountry` int(11) NOT NULL,
  `countryName` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dataark for tabell `country`
--

INSERT INTO `country` (`idCountry`, `countryName`) VALUES
(1, 'Korea'),
(2, 'Canada'),
(3, 'USA'),
(4, 'Greenland'),
(5, 'Denmark'),
(6, 'Japan'),
(7, 'Norway'),
(8, 'Russia'),
(9, 'China'),
(10, 'Switzerland'),
(11, 'Germany'),
(12, 'Italy'),
(13, 'Sweden');

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `countrycooperation`
--

CREATE TABLE `countrycooperation` (
  `idCountryCoop` int(11) NOT NULL,
  `countryId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dataark for tabell `countrycooperation`
--

INSERT INTO `countrycooperation` (`idCountryCoop`, `countryId`) VALUES
(1, 1),
(2, 2),
(3, 3),
(4, 4),
(5, 5),
(6, 6),
(7, 7),
(8, 8),
(9, 9),
(10, 10),
(11, 11),
(12, 12),
(13, 13),
(101, 2),
(101, 3),
(101, 4),
(101, 5),
(102, 2),
(102, 3),
(102, 6),
(103, 7),
(103, 8),
(104, 8),
(104, 10),
(104, 11);

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `mission`
--

CREATE TABLE `mission` (
  `idMission` int(11) NOT NULL,
  `missionName` varchar(60) DEFAULT NULL,
  `startOfMissionDate` date NOT NULL,
  `endOfMissionDate` date NOT NULL,
  `contactPerson` int(11) DEFAULT NULL,
  `region` varchar(120) DEFAULT NULL,
  `shipId` int(11) NOT NULL,
  `countryCoop` int(11) NOT NULL,
  `Link` varchar(60) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dataark for tabell `mission`
--

INSERT INTO `mission` (`idMission`, `missionName`, `startOfMissionDate`, `endOfMissionDate`, `contactPerson`, `region`, `shipId`, `countryCoop`, `Link`) VALUES
(1, 'SAS', '2020-08-08', '2020-09-04', 1, 'Bering Strait, Chukchi Sea, East Siberian Sea, Chukchi Borderland, Makarov Basin', 9490935, 1, 'https://kaos.kopri.re.kr/'),
(10, 'SAS/ArCSII', '2021-08-31', '2021-10-22', 6, 'Bering and Chukchi seas, and Canada Basin', 6919423, 6, 'https://ads.nipr.ac.jp/');

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `ship`
--

CREATE TABLE `ship` (
  `registration` int(11) NOT NULL,
  `shipName` varchar(45) NOT NULL,
  `shipType` varchar(45) DEFAULT NULL,
  `countryId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dataark for tabell `ship`
--

INSERT INTO `ship` (`registration`, `shipName`, `shipType`, `countryId`) VALUES
(6705937, 'LOUIS S.ST LAURENT', 'Icebreaker', 2),
(6919423, 'MIRAI', 'Research/Survey Vessel', 6),
(7912680, 'DANA', 'Fishery Research Vessel', 5),
(8227056, 'VILNYUS', 'Fishing vessel', 8),
(8700876, 'ODEN', 'Icebreaker', 13),
(8716655, 'HELMER HANSSEN', 'Trawler', 7),
(8915768, 'JOHAN HJORT', 'Fishery Research Vessel', 7),
(9083380, 'HEALY', 'Icebreaker', 3),
(9114256, 'LAURA BASSI', 'Research/Survey Vessel', 12),
(9260316, 'G.O.SARS', 'Research/Survey Vessel', 7),
(9274197, 'MARIA S.MERIAN', 'Research/Survey Vessel', 11),
(9490935, 'ARAON', 'Icebreaker', 1),
(9548536, 'AKADEMIK TRYOSHNIKOV', 'Icebreaker', 8),
(9688946, 'NEIL ARMSTRONG', 'Research/Survey Vessel', 3),
(9739587, 'KRONPRINS HAAKON', 'Research/Survey Vessel', 7),
(220428000, 'Knud Rasmussen', 'warship', 5);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `contactperson`
--
ALTER TABLE `contactperson`
  ADD PRIMARY KEY (`idContact`),
  ADD UNIQUE KEY `idContact` (`idContact`);

--
-- Indexes for table `coordinate`
--
ALTER TABLE `coordinate`
  ADD PRIMARY KEY (`coordinateNr`,`missionId`),
  ADD KEY `MissionCoordinates` (`missionId`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`idCountry`),
  ADD UNIQUE KEY `idCountry` (`idCountry`);

--
-- Indexes for table `countrycooperation`
--
ALTER TABLE `countrycooperation`
  ADD PRIMARY KEY (`idCountryCoop`,`countryId`),
  ADD KEY `Country` (`countryId`);

--
-- Indexes for table `mission`
--
ALTER TABLE `mission`
  ADD PRIMARY KEY (`idMission`),
  ADD UNIQUE KEY `idMission` (`idMission`),
  ADD KEY `shipMissionRelation` (`shipId`),
  ADD KEY `ResearchMissionRelation` (`contactPerson`),
  ADD KEY `CountryMissionRelation` (`countryCoop`);

--
-- Indexes for table `ship`
--
ALTER TABLE `ship`
  ADD PRIMARY KEY (`registration`),
  ADD UNIQUE KEY `registration` (`registration`),
  ADD KEY `shipOrigin` (`countryId`);

--
-- Begrensninger for dumpede tabeller
--

--
-- Begrensninger for tabell `coordinate`
--
ALTER TABLE `coordinate`
  ADD CONSTRAINT `MissionCoordinates` FOREIGN KEY (`missionId`) REFERENCES `mission` (`idMission`);

--
-- Begrensninger for tabell `countrycooperation`
--
ALTER TABLE `countrycooperation`
  ADD CONSTRAINT `Country` FOREIGN KEY (`countryId`) REFERENCES `country` (`idCountry`);

--
-- Begrensninger for tabell `mission`
--
ALTER TABLE `mission`
  ADD CONSTRAINT `CountryMissionRelation` FOREIGN KEY (`countryCoop`) REFERENCES `countrycooperation` (`idCountryCoop`),
  ADD CONSTRAINT `ResearchMissionRelation` FOREIGN KEY (`contactPerson`) REFERENCES `contactperson` (`idContact`),
  ADD CONSTRAINT `shipMissionRelation` FOREIGN KEY (`shipId`) REFERENCES `ship` (`registration`);

--
-- Begrensninger for tabell `ship`
--
ALTER TABLE `ship`
  ADD CONSTRAINT `shipOrigin` FOREIGN KEY (`countryId`) REFERENCES `country` (`idCountry`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
